class Api::V1::AuthenticationController < ApplicationController
  def create
    if user.authenticate(params[:password])
       render json: generate_token, status: :ok
    else
      render json: {}, status: :unauthorized
    end
  end

  private

  def user 
    @user ||= User.find_by_email(params[:email])
  end

  def generate_token
    authToken = loop do
      random_token = SecureRandom.hex
      break random_token unless Token.exists?(token: random_token)
    end
    
    token = Token.new
    token.user_id = @user.id
    token.token = authToken
    token.save
    authToken
  end
end