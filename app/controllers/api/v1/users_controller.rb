class Api::V1::UsersController < ApplicationController
    before_action :find_user, :except => [:index, :create]
    before_action :authenticate, :except => [:create]

    def find_user
        @user = User.find_by_id(params[:id])
    end

    def index
        @users = User.all
        render json: @users
    end

    def show
        respond_to do |format|
          format.json { render json: @user }
          format.xml { render xml: @user }
        end
    end

    def create
        @user = User.new(
            user_name: params[:user_name], 
            email: params[:email], 
            password: params[:password]
        )

        if @user.save
            render json: @user
        else
            render json: {error: 'error during creating user'}
        end
    end

    def update
        @user.email = params[:email]
        @user.user_name = params[:user_name]
        @user.password = params[:password]
        if @user.save
            render json: @user
        else
            render json: {error: 'error during user update'}
        end
    end

    def destroy
        if @user.destroy
            render json: {status: 'ok'}
        else
            render json: {error: 'error during user delete'}
        end
    end

    protected

    def authenticate
      authenticate_or_request_with_http_token do |token, options|
        Token.find_by(token: token)
      end
    end
end
